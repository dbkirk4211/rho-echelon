# Pit Rho Fantasy
Pit Rho's fantasy NASCAR page.

# Setup

Uses [Brunch](http://brunch.io) for asset compilation/minification.
Written using [CoffeeScript](http://coffeescript.org)
and [LESS](http://lesscss.org/).
Uses [Bootstrap 3](http://getbootstrap.com).

## Requirements

1. Brunch (`npm install -g brunch/brunch`)
1. Bower (`npm install -g bower`)

## Install

    npm install
    bower install

# Usage

## Develop

	npm start

Navigate to `localhost:3333`

## Build for Production

`npm run-script build-production` will compile page to `./public` directory. Upload all contents
to static hosting of choice or open locally.
