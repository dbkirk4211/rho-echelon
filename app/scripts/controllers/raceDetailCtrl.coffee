app = angular.module('prfApp') # Get our module

app.controller('raceDetailCtrl',
['$scope', '$routeParams', '$filter', '$location', 'raceFactory', 'book'
($scope, $routeParams, $filter, $location, raceFactory, book) ->
  # Initialize variables
  $scope.year = parseInt($routeParams.year)
  $scope.race_number = parseInt($routeParams.racenumber)
  $scope.series_id = $routeParams.seriesid

  ###
  $scope.wagers = []
  book.setBook($scope.year, $scope.race_number, $scope.series_id).then (promise) ->
    $scope.wagers = promise.data
    ###

  $scope.book = book


  $scope.datetime =
    value: new Date()
    matchup: false
    win: false
    group: false
    edit: false

    clear: ->
      $scope.datetime.matchup = false
      $scope.datetime.win = false
      $scope.datetime.group = false
      $scope.datetime.edit = false

    add: (wagerType) ->
      $scope.datetime[wagerType] = true
      $scope.datetime.edit = true

    save: (wagerType) ->
      $scope.datetime[wagerType] = false
      $scope.datetime.edit = false
      book.addDateTime($scope.datetime.value)

    cancel: (wagerType) ->
      $scope.datetime[wagerType] = false
      $scope.datetime.edit = false

  $scope.datetime.value.setMilliseconds(0)
  $scope.datetime.value.setSeconds(0)

  $scope.echelon = 
    lines: []
    bets: []
    odds: []
    dates: []
    wagers: []
    wagers2: []

    addBet: (driverId, odds, datetime) ->
      console.log odds
      $scope.echelon.bets.push
        driverId: driverId
        betType: $scope.edit.filter
        odds: odds
        placed: false
        amount: 0
        datetime: datetime
        won: null
      
  betTypeSizes =
    win:1
    matchup:2
    group:4

  $scope.edit =
    left: -170
    filter: ""
    visible: false
    name: ""

    add: (driverId) ->
      $scope.book.addLine $scope.edit.filter, driverId
      #wager = $scope[$scope.edit.filter].newWager(driverId)
      #$scope.echelon.wagers.push wager
      ###
      wager2 = $scope[$scope.edit.filter].newWager2(driverId)
      if $scope.echelon.wagers2.length is 0
        $scope.echelon.wagers.push wager2
      else
        z = betTypeSizes[$scope.edit.filter]
        saved = false

        x = 0
        while x < $scope.echelon.wagers2.length and not saved
          if $scope.echelon.wagers2[x].type is $scope.edit.filter
            if $scope.echelon.wagers2[x].drivers.length isnt z
              saved = true
              $scope.echelon.wagers2[x].drivers.push wager2.drivers[0]

          x++
        if not saved
          $scope.echelon.wagers2.push wager2
          ###

    click: (tab) ->
      $scope.datetime.clear()
      if tab isnt "settings"

        if $scope.edit.filter is "settings"
          $scope.edit.left = $scope.edit.left * -1 - 170

        if $scope.edit.visible
          if tab is $scope.edit.filter
            $scope.edit.left = $scope.edit.left * -1 - 170
            $scope.edit.filter = ""
            $scope.edit.visible = false
          else
            $scope.edit.filter = tab

        else
          $scope.edit.left = $scope.edit.left * -1 - 170
          $scope.edit.filter = tab
          $scope.edit.visible = true
      else
        if $scope.edit.filter is ""
          $scope.edit.visible = true
          $scope.edit.filter = tab
        else if $scope.edit.filter is "settings"
          $scope.edit.filter = ""
          $scope.edit.visible = false
        else
          $scope.edit.left = $scope.edit.left * -1 - 170
          $scope.edit.filter = tab

    clear: ->
      $scope.edit.name = ""

  $scope.mod = (num, divBy) ->
    return num % divBy







  setLineWagers = (lines) ->
    for line in lines
      do (line) ->
        line["wager"] = if $scope.echelon.wagers.indexOf(line.lineId) isnt -1 then true else false
        return line







  $scope.sortingOn = false
  $scope.sortCol = 0
  $scope.sortDir = false

  $scope.setSort = (col) ->
    if not $scope.visual.selected.allowSorting then return
    if col is $scope.sortCol
      $scope.sortDir = not $scope.sortDir
    else
      $scope.sortCol = col

  $scope.getCellValue = (cell) ->
    if cell.length is 0
      return null
    else
      return cell[$scope.sortCol].value

  $scope.getCellType = (cell) ->
    if cell.length is 0
      return null
    else if not $scope.sortDir
      return if cell[$scope.sortCol].type.substr(0,3) is "pre" then 1 else 0
    else
      return if cell[$scope.sortCol].type.substr(0,3) is "pre" then 0 else 1
      
  $scope.getFirstCell = (cell) ->
    if cell.length is 0
      return null
    else
      return cell[0].value

  $scope.normalize = (v) ->
    while v.toString().length < 4
      v = "0" + v
    return v

  setRace = ->
    tempRace = $filter('filter')($scope.raceFactory.races(),{year:$scope.year,race_number:$scope.race_number,series_id:$scope.series_id},true)
    if angular.isUndefined tempRace
      $location.url('/')
    else if tempRace.length > 0
      $scope.race.select(tempRace[0])
      $scope.book.setBook($scope.year, $scope.race_number, $scope.series_id)
    else
      $location.url('/')

  if not $scope.raceFactory.isRacesLoaded()
    $scope.raceFactory.getRaces().then (promise) ->
      setRace()
  else
    setRace()
])
