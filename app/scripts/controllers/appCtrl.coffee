app = angular.module('prfApp') # Get our module

app.controller('appCtrl',
['$rootScope', '$scope', '$route','raceFactory', '$filter', 'bankroll'
($rootScope, $scope, $route, raceFactory, $filter, bankroll) ->
  
  $scope.route = $route
  $scope.nav = {
    'isCollapsed':  true,
    'collapse': -> $scope.nav.isCollapsed = true,
    'toggle': -> $scope.nav.isCollapsed = !$scope.nav.isCollapsed
  }

  #Loads Races From Pit Rho's Races API
  #
  $scope.raceFactory = raceFactory  

  #Stops the propagation of a click to close menus if a menu button has class stopclick
  #
  $scope.htmlClick = ($event) ->
    ep = $event.path
    i = 0

    while i < ep.length
      if ep[i].hasOwnProperty "className"
        if ep[i].className.indexOf("stopclick") isnt -1
          return
      i++

    $scope.race.year.isVisible = false
    $scope.race.series.isVisible = false
    $scope.race.sort.isVisible = false


   
  #Race Data Vars
  $scope.raceInRaces = (race) ->
    i = 0
    while i < $scope.race.history.races.length
      hrace = $scope.race.history.races[i]
      return true if (race.year + " " + race.race_number + " " + race.series_id) is (hrace.year + " " + hrace.race_number + " " + hrace.series_id)
      i++
    false

  $scope.race =
    "hasData": false,
    "hasPostData": false,
    "hasPreData": false,
    "hasPostDriversData": false,
    "hasPostTeamsData": false,
    "hasPreDriversData": false,
    "hasPreTeamsData": false,
    "history" : {
      "races":[],
      "addRace": (race) ->
        $scope.race.history.races.unshift(race) unless $scope.raceInRaces race
        $scope.race.history.races.pop() if $scope.race.history.races.length > 4
        return

    },
    "drivers": {},
    "isSelected": (race) ->
      return if (race.year + " " + race.race_number + " " + race.series_id) is ($scope.race.selected.year + " " + $scope.race.selected.race_number + " " +  $scope.race.selected.series_id) then true else false
    "selected":{"id":-1},
    "select":(race) ->
      $scope.race.selected = race
      

      raceFactory.getRaceData(race.year,race.race_number,race.series_id).then (promise) ->
        $scope.race.hasData = promise.data.hasData
        $scope.race.hasPostData = promise.data.hasPostData
        $scope.race.hasPreData = promise.data.hasPreData
        
        $scope.race.hasPostDriversData = promise.data.hasPostDriversData
        $scope.race.hasPostTeamsData = promise.data.hasPostTeamsData
        $scope.race.hasPreDriversData = promise.data.hasPreDriversData
        $scope.race.hasPreTeamsData = promise.data.hasPreTeamsData

        $scope.race.drivers = promise.data.drivers
        $scope.race.driversAr = (promise.data.drivers[driver] for driver of promise.data.drivers)

        $rootScope.title = "Race #" + race.race_number + " at " + race.track.name + " in the " + race.year + " NASCAR Sprint Cup Series - Pit Rho"

      $scope.race.history.addRace(race)
      return
    "year" : {
      "isVisible":false,
      "selected":2015,
      "select":(year) ->
        $scope.race.year.selected = year
        $scope.race.year.isVisible = false
        return
    },"series": {
      "isVisible":false,
      "selected":"Sprint",
      "select": (series) ->
        $scope.race.series.selected = series
        $scope.race.series.isVisible = false
        return
    },"sort": {
      "isVisible":false,
      "selected":{"name":"Oldest","icon":"fa-sort-numeric-asc","filter":{"p":"scheduled_datetime","r":false}},
      "sortings": [
        {"name":"Oldest","icon":"fa-sort-numeric-asc","filter":{"p":"scheduled_datetime","r":false}},
        {"name":"Latest","icon":"fa-sort-numeric-desc","filter":{"p":"scheduled_datetime","r":true}},
        {"name":"By Race","icon":"fa-sort-alpha-asc","filter":{"p":"name","r":false}},
        {"name":"By Track","icon":"fa-sort-alpha-asc","filter":{"p":"track.name","r":false}}],
      "select": (sort) ->
        $scope.race.sort.selected = sort
        $scope.race.sort.isVisible = false
        return
    },"toggle": (key) ->
      return if not $scope.race.hasOwnProperty(key)
      $scope.race[key].isVisible = not $scope.race[key].isVisible
      $scope.race.year.isVisible = false if key isnt "year"
      $scope.race.series.isVisible = false if key isnt "series"
      $scope.race.sort.isVisible = false if key isnt "sort"


  return
])