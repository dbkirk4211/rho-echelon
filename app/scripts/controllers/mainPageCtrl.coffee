app = angular.module('prfApp') # Get our module

app.controller('mainPageCtrl', ['$scope', '$routeParams', ($scope, $routeParams) ->
  year = $routeParams.year
  if year? and not isNaN(parseInt(year))
      $scope.race.year.selected = parseInt(year)
])
