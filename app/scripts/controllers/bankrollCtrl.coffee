app = angular.module('prfApp') # Get our module

app.controller('bankrollCtrl', ['$scope','bankroll', 'book', ($scope, bankroll, book) ->

  $scope.bankroll = bankroll
  $scope.bankroll.fetch()

  $scope.filters = 
    credits: true
    debits: true
    winners: true
    bets: true

    match: (transaction) ->
      if $scope.filters.credits is false and transaction.type is 'Credit' then return false
      if $scope.filters.debits is false and transaction.type is 'Debit' then return false
      if $scope.filters.winners is false and transaction.type is 'Winner' then return false
      if $scope.filters.bets is false and transaction.type is 'Bet' then return false
      return true

  $scope.transaction = 
    active: false
    datetime: new Date()
    types: [{label:'Credit'},{label:'Debit'}]
    type: 'Credit'
    amount: null

    keypress: (e) ->
      if e.which is 13
        $scope.transaction.save()

    new: ->
      if not $scope.transaction.active
        $scope.transaction.active = true
      else
        $scope.transaction.close()

    save: ->
      if $scope.transaction.amount isnt null and $scope.transaction.amount isnt 0
        $scope.bankroll.add($scope.transaction.datetime, $scope.transaction.type, $scope.transaction.amount)

      $scope.transaction.close()

    close: ->
      $scope.transaction.active = false
      $scope.transaction.amount = null

  $scope.transaction.datetime.setMilliseconds(0)
  $scope.transaction.datetime.setSeconds(0)

  ###$scope.transaction = 
    new:
      show:false
      date: new Date()
      type:"Credit"
      amt:null
      month: new Date().getMonth()

    add: ->
      $scope.bankroll.new.show = true
    cancel: ->
      $scope.bankroll.new.show = false
    save: ->
      n = $scope.bankroll.new
      bankroll.add n.date, n.type, n.amt
      $scope.bankroll.transactions = bankroll.getTransactions()
      $scope.bankroll.new.show = false###
  


])
