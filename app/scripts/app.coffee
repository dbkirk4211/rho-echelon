# Define primary application Modules & Routes
app = angular.module('prfApp', ['ngRoute', 'highcharts-ng', 'ui.bootstrap.datetimepicker'])


app.config(['$locationProvider', '$routeProvider', ($locationProvider, $routeProvider) ->
    $locationProvider.hashPrefix('!')
    $routeProvider.
      when('/', {
        templateUrl: 'partials/main-page.html',
        controller: 'mainPageCtrl',
        activetab: 'home'
      }).
      when('/bankroll', {
        templateUrl: 'partials/bankroll.html',
        controller: 'bankrollCtrl',
        activetab: 'bankroll'
      }).
      when('/:year/:racenumber/:seriesid', {
        templateUrl: 'partials/race-detail.html',
        controller: 'raceDetailCtrl',
        activetab: 'race'
      }).
      when('/:year', {
        templateUrl: 'partials/main-page.html',
        controller: 'mainPageCtrl',
        activetab: 'home'
      }).
      otherwise({
        redirectTo: '/'
      })
  ])