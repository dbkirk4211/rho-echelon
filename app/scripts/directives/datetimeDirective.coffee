app = angular.module('prfApp')
app.directive 'datetimeFilter', ['$filter', ($filter) ->
  {
    require: 'ngModel'
    link: (scope, element, attrs, ngModelController) ->
      ngModelController.$parsers.push (data) ->
        return new Date(data)

      ngModelController.$formatters.push (data) ->
        return $filter('date')(data,'short')

      return
  }]