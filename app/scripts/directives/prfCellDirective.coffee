app = angular.module('prfApp')
app.directive "prfcell", ->
  return {
    replace: true,
    restrict: "A",
    link: (scope, elem, attr) ->
      if attr.celltype.indexOf "--"
        types = attr.celltype.split "--"
      else
        types = []
        types.push attr.celltype

      cssAr = []
      i = 0
      while i < types.length
        if types[i] is "hidden"
          elem.remove()
          i = types.length
        else if types[i] is "text"
          elem.text = attr.cellvalue
        else if types[i].indexOf("rowspan") isnt -1
          elem.attr("rowspan",types[i].split("-")[1])
        else if types[i].indexOf("colspan") isnt -1
          elem.attr("colspan",types[i].split("-")[1])
        else if types[i].indexOf("bold") isnt -1
          cssAr.push "font-weight:bold"
        else if types[i].indexOf("subtable") isnt -1
          cssAr.push "font-weight:bold"
          cssAr.push "padding-top:50px"
          cssAr.push "padding-bottom:20px"
          cssAr.push "border-left:none"
          cssAr.push "border-right:none"
        else if types[i].indexOf("color") isnt -1
          cssAr.push "color:" + types[i].split("-")[1]

        if cssAr.length isnt 0
          elem.attr("style","text-align:center;vertical-align:middle;" + cssAr.join(";"))
        else
          elem.attr("style","text-align:center;vertical-align:middle")
        i++
  }