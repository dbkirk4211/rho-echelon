app = angular.module('prfApp')
app.directive 'lineNumber', ['$filter','$parse', ($filter, $parse) ->
  {
    require: 'ngModel'
    scope:
      change: "="

    link: (scope, element, attrs, ngModelController) ->
      scope.$watch(attrs.ngModel, (number) ->
        console.log number
        $parse(attrs.change).assign(scope, number + 1)

      )
      
      return
  }]