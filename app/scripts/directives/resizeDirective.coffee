app = angular.module('prfApp')
app.directive "resize", ["$window", ($window) ->
  (scope, element, attr) ->
    w = angular.element($window)
    scope.$watch (->
      h: window.innerHeight
      w: window.innerWidth
    ), ((newValue, oldValue) ->
      scope.windowHeight = newValue.h
      scope.windowWidth = newValue.w
      return
    ), true
    w.bind "resize", ->
      scope.$apply()
      return
    return
  ]