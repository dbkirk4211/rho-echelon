app = angular.module('prfApp')
app.filter "parseTitleCase", [
  "$filter"
  ($filter) ->
    return (input) ->

      if angular.isString input
        return input.split(/(?=[A-Z])/).join(" ")
        
      return input
]