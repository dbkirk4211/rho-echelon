app = angular.module('prfApp')
app.filter "historyRaceTab", [
  "$filter"
  ($filter) ->
    return (race) ->

      if angular.isObject race
        return race.track.name + " " + $filter('date')(race.scheduled_datetime,"MMM ''''yy")
        
      return "Race"
]