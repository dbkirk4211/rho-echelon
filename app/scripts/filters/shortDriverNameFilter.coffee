app = angular.module('prfApp')
app.filter "shortDriverName", [
  "$filter"
  ($filter) ->
    return (input) ->

      if angular.isString input
        inputAr = input.split(" ")

        inputAr[1] = inputAr[1].substring(0,1) + "."
        
        if inputAr.length is 3
          inputAr[1] = inputAr[1] + " " + inputAr[2]

        return inputAr[0] + " " + inputAr[1]

      return input
]