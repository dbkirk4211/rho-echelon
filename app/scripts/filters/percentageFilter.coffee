app = angular.module('prfApp')
app.filter "percentage", [
  "$filter"
  ($filter) ->
    return (input, decimals) ->

      if angular.isNumber input
        return $filter('number')(input * 100, decimals) + "%"
        
      return input
]