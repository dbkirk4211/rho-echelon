app.factory "bankroll", [
  "$http", '$filter', '$rootScope', 'book',
  ($http, $filter, $rootScope, book) ->
    loaded = false

    combined = []
    transactions = []
    placedBets = []

    #Delete After Backend - Start
    $rootScope.book = book
    $rootScope.$watch(
      -> $rootScope.book.wagers()
      (newVal) -> parseWagers(newVal)
    ,true)
    #Delete After Backend - End

    $rootScope.$watch(
      -> transactions
      -> combined = transactions.concat(placedBets)
      true)
    $rootScope.$watch(
      -> placedBets
      -> combined = transactions.concat(placedBets)
      true)
    $rootScope.$watch(
      -> combined
      -> calcTotals()
      true)

    total = 0
    totalInPlay = 0
    totalPayout = 0

    fetch = ->
      json = "json/transactions.json"
      
      $http(
        method: "get"
        url: json
        cache: true
      ).success (data, status) ->
        transactions = ( formatTransaction transaction for transaction in data )
        loaded = true
      
      #Get All Placed Bets
      json = "json/book.json"
      
      $http(
        method: "get"
        url: json
        cache: false
      ).success (data, status) ->
        #parseBets(data)

    calcPayout = (amt, ml) ->
      if ml < 0
        return ((100 / Math.abs(ml)) + 1) * amt
      else
        return ((100 / ml) + 1) * amt

    calcTotals = ->
      total = 0
      totalInPlay = 0
      totalPayout = 0

      x = 0
      while x < combined.length
        t = combined[x]
        if t.type is "Bet" or t.type is "Debit"
          total -= t.amount
        else
          total += t.amount

        if t.type is "Bet"
          totalInPlay += t.amount
          console.log t.moneyLine
          totalPayout += calcPayout(t.amount, t.moneyLine)

        x++


    parseWagers = (wagers) ->
      placedBets = []
      x = 0
      while x < wagers.length
        lines = wagers[x].lines

        y = 0
        while y < lines.length
          bets = lines[y].bets

          z = 0
          while z < bets.length
            bet = bets[z]
            #console.log bet

            if bet.placed
              placedBets.push
                id: bet.id
                type: "Bet"
                amount: bet.amount
                datetime: bet.dateTime
                moneyLine: bet.moneyLine
            z++
          y++
        x++


    formatTransaction = (transaction) ->
      transaction.datetime = new Date(transaction.datetime)
      return transaction

    add = (datetime, type, amount) ->
      json = "http://fantasyapi.pitrho.com/api/fantasy/races"
      
      $http(
        method: "get"
        url: json
        cache: true
      ).success (data, status) ->
        transactions.push
          datetime: new Date(datetime)
          type: type
          amount: amount

    return (
      fetch: fetch
      loaded: -> loaded

      transactions: -> combined

      total: -> total
      totalInPlay: -> totalInPlay
      totalPayout: -> totalPayout
      add: add
    )
  ]