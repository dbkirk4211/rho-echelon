app.factory "book", [
  "$http", '$filter', '$rootScope'
  ($http, $filter, $rootScope) ->
    
    #API Will Give These IDs
    wagerId = 5
    lineId = 9
    betId = 9

    #Delete After Backend - Start
    race =
      year: 0
      number: 0
      series: ""
    #Delete After Backend - End

    lineNumber = 
      matchup:
        count: 0
        start: 6803
      win:
        count: 0
        start: 6203
      group:
        count: 0
        start: 8605

    wagers = []
    dateTimes = []

    wagerLength = 
      group: 4
      win: 1
      matchup: 2


    genLineNumber = (wagerType) ->
      switch wagerType
        when "matchup" then lineNumber.matchup.start + lineNumber.matchup.count++
        when "win" then lineNumber.win.start + lineNumber.win.count++
        when "group" then lineNumber.group.start + lineNumber.group.count++ #lineNumber.group.start + (20 * Math.floor(lineNumber.group.count / 4)) + (lineNumber.group.count++ % 4)

    setBook = (raceYear, raceNumber, raceSeries) ->
      #Delete After Backend - Start
      if raceYear is race.year and raceNumber is race.number and raceSeries is race.series
        return
      
      race.year = 2015
      race.number = 1
      race.series = "W"
      #Delete After Backend - End
      
      json = "http://echelonapi.pitrho.com/2014/01/W"
      json = "json/book.json"
      
      $http(
        method: "get"
        url: json
        cache: false
      ).success (data, status) ->
        wagers = data
        buildDates();int()

    buildDates = ->
      x = 0
      while x < wagers.length
        lines = wagers[x].lines

        y = 0
        while y < lines.length
          bets = lines[y].bets

          z = 0
          while z < bets.length
            bet = bets[z]

            addDateTime bet.dateTime
            z++

          y++

        x++

    add = ->
      null

    addDateTime = (dateTime) ->
      present = false
      x = 0
      while x < dateTimes.length
        if dateTime is dateTimes[x]
          present = true  
          x = dateTimes.length
        x++

      if not present
        dateTimes.push dateTime

    addWager = (wagerType, driver) ->
      wagers.push
        id: null,
        type: wagerType,
        valid: false,
        lines: []
      addLine wagerType, driver

      ###console.log driver
      #Send JSON to New Wager API with RaceID and WagerType, Response will be a Wager Object
      json = "http://fantasyapi.pitrho.com/api/fantasy/2014/01/W"
      $http(
        method: "get"
        url: json
        cache: false
      ).success (data, status) ->
        #Send JSON to New Line API with WagerID, DriverID and LineNumber, Response will be Line Object
        json = "http://fantasyapi.pitrho.com/api/fantasy/2014/01/W"
        $http(
          method: "get"
          url: json
          cache: false
        ).success (data, status) ->
          console.log wagers
          wagers.push
            id: wagerId++,
            type: wagerType,
            valid: false,
            lines: [
              {
                id: lineId++,
                number: 6801,
                driverId: driver.id,
                bets: []
              }
            ]
          console.log wagers###
        

    addLine = (wagerType, driver) ->
      maxLength = wagerLength[wagerType]

      x = 0
      while x < wagers.length
        wager = wagers[x]
        if wager.type is wagerType and wager.lines.length isnt maxLength
          
          wager.lines.push
              id: 0,
              number: genLineNumber(wagerType),
              driverId: driver.id,
              bets: []

          if wager.lines.length is maxLength
            #wager.valid = true

            #Send JSON to New Line API with WagerID, DriverID and LineNumber, Response will be Line Object
            json = "http://fantasyapi.pitrho.com/api/fantasy/2014/01/W"
            $http(
              method: "get"
              url: json
              cache: false
            ).success (data, status) ->
              console.log x, wagers, wagers[x]
              wager.id = wagerId++
              line.id = lineId++ for line in wager.lines

          x = wagers.length
        x++

      if x is wagers.length
        addWager wagerType, driver

    addBet = (wager, line, moneyLine, dateTime) ->
      #Send JSON to New Line API with WagerID, DriverID and LineNumber, Response will be Line Object
      json = "http://fantasyapi.pitrho.com/api/fantasy/2014/01/W"
      $http(
        method: "get"
        url: json
        cache: false
      ).success (data, status) ->
        line.bets.push
          betId: betId++,
          dateTime: dateTime,
          moneyLine: moneyLine,
          amount: 0,
          placed: false,
          won: null,
          calculated: false


        betCnt = 0
        for line in wager.lines
          for bet in line.bets
            if bet.dateTime is dateTime
              betCnt++

        if wagerLength[wager.type] is betCnt
          wager.valid = true
          calcBets(wager, line, dateTime)


    calcBets = (wager, line, dateTime) ->
      console.log "Send To Server"
      #Send JSON to New Line API with WagerID, DriverID and LineNumber, Response will be Line Object
      json = "http://fantasyapi.pitrho.com/api/fantasy/2014/01/W"
      $http(
        method: "get"
        url: json
        cache: false
      ).success (data, status) ->
        for line in wager.lines
          for bet in line.bets
            if bet.dateTime is dateTime
              bet.amount = Math.floor(Math.random() * 40)
              bet.calculated = true

    placeBet = (bet) ->
      #Send JSON to New Line API with WagerID, DriverID and LineNumber, Response will be Line Object
      json = "http://fantasyapi.pitrho.com/api/fantasy/2014/01/W"
      $http(
        method: "get"
        url: json
        cache: false
      ).success (data, status) ->
        bet.placed = true


    updateLineNumber = (line) ->
      #Send JSON to New Line API with WagerID, DriverID and LineNumber, Response will be Line Object
      json = "http://fantasyapi.pitrho.com/api/fantasy/2014/01/W"
      $http(
        method: "get"
        url: json
        cache: false
      ).success (data, status) ->
        console.log line

    updateLineDriver = (line) ->
      #Send JSON to New Line API with WagerID, DriverID and LineNumber, Response will be Line Object
      json = "http://fantasyapi.pitrho.com/api/fantasy/2014/01/W"
      $http(
        method: "get"
        url: json
        cache: false
      ).success (data, status) ->
        console.log line

    return (
        setBook: setBook
        wagers: -> wagers
        dateTimes: -> dateTimes
        addDateTime: addDateTime

        addWager: addWager
        addLine: addLine
        addBet: addBet
        placeBet: placeBet

        updateLineNumber: updateLineNumber
        updateLineDriver: updateLineDriver
    )
  ]