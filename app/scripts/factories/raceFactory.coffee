app.factory "raceFactory", [
  "$http", '$filter'
  ($http, $filter) ->

    ###Data Calls To Pit Rho Fantasy API###

    #Races
    races = []
    racesLoaded = false
    getRaces = ->
      json = "http://fantasyapi.pitrho.com/api/fantasy/races"
      
      $http(
        method: "get"
        url: json
        cache: true
      ).success (data, status) ->
        races = ( newRace race for race in data.races )
        racesLoaded = true

    getRaces()


    #Data Storage
    drivers = {}
    driversAr = []

    race = 
      year: null
      number: null
      seriesId: null

    getRaceData = (raceYear, raceNumber, raceSeriesId) -> 
      race.year = raceYear
      race.number = raceNumber
      race.seriesId = raceSeriesId

      json = "http://fantasyapi.pitrho.com/api/fantasy/" + race.year + "/" + race.number + "/" + race.seriesId
      
      $http(
        method: "get"
        url: json
        cache: true
      ).success (data, status) ->        
        drivers = data.drivers
        
        if data.fantasy_stats.hasOwnProperty("post_qualifying")
          addStats(drivers, data.fantasy_stats.post_qualifying.fantasy_drivers)
        else if data.fantasy_stats.hasOwnProperty("pre_qualifying")
          addStats(drivers, data.fantasy_stats.pre_qualifying.fantasy_drivers)

        driversAr = []
        for driver of drivers
          if drivers.hasOwnProperty(driver)
            if not drivers[driver].hasOwnProperty('efp')
              drivers[driver]['efp'] = 100
            driversAr.push drivers[driver] 

    addStats = (drivers, stats) ->
      for driver of drivers
        if not drivers[driver].hasOwnProperty('efp')
            drivers[driver]['efp'] = 100
        if stats.hasOwnProperty(driver)
          drivers[driver].efp = stats[driver].expected_finish_position
        else
          drivers[driver].efp = 100


    #Given string, returns value with the first letter of each word capitalized
    #
    uppercase = (s) ->
      s = (if (s is 'undefined' or s is null) then "" else s)
      s.toString().toLowerCase().replace /\b([a-z])/g, (ch) ->
          ch.toUpperCase()


    #Given a race object from Pit Rho's API, returns race object with properly formatted series name and race data
    #
    newRace = (race) ->
      race.series_short_name = uppercase(race.series_short_name)
      race.scheduled_datetime = new Date(race.scheduled_datetime)
      return race

    return (
      getRaceData: getRaceData
      getRaces: getRaces
      race: -> race
      isRacesLoaded: -> racesLoaded
      races: -> races
      drivers: -> drivers
      driversAr: -> driversAr
    )
]