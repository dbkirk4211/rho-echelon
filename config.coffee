exports.config =
  # See http://brunch.io/#documentation for docs.
  jsWrapper: 'raw'
  files:
    javascripts:
      joinTo:
        'js/app.js': /^(app)/
        'js/vendor.js': /^(bower_components)/
        'test/js/test.js': /^test(\/|\\)(?!bower_components)/
        'test/js/test-bower_components.js': /^test(\/|\\)(?=bower_components)/
      order:
        before: ['app/scripts/app.coffee']

    stylesheets:
      joinTo:
        'css/app.css' : /^(bower_components|app)/

  modules:
    wrapper: false
    definition: false
    nameCleaner: (path) ->
      path.replace(/^app\/scripts\//, '')

  templates:
      joinTo:
        'js/' : /^app/ # dirty hack for Jade compiling.

  paths:
    public: './public'
    watched: ['app']

  minify: true
